with ANSI;
with Ada.Characters.Handling;
with Ada.Text_IO;

procedure Cursor_Position is
   Buffer : Character;

   Line, Column : Natural;

   procedure Type_Message (Message : in String; Separation : Duration);

   procedure Type_Message (Message : in String; Separation : Duration) is
   begin
      for Letter in Message'Range loop
         if Ada.Characters.Handling.Is_Graphic (Message (Letter) ) then
            Ada.Text_IO.Put (Message (Letter) );

            delay Separation;
         end if;
      end loop;
   end Type_Message;
begin -- Cursor_Position
   ANSI.Move_Cursor (23, 10);
   ANSI.Get_Cursor_Position (Column => Column, Line => Line);
end Cursor_Position;
