with ANSI;
with Ada.IO_Exceptions;
with Ada.Numerics.Discrete_Random;
with Ada.Text_IO;

procedure General is
   subtype Chars is Character range ' ' .. '~';

   package Random_Colors is new Ada.Numerics.Discrete_Random (ANSI.Color);
   package Random_Characters is new Ada.Numerics.Discrete_Random (Chars);

   package ANSI_Color_IO is new Ada.Text_IO.Enumeration_IO (ANSI.Color);

   Color_Gen : Random_Colors.Generator;
   Char_Gen  : Random_Characters.Generator;

   Mode      : ANSI.Graphic_Mode;
   Put_Color : ANSI.Color;
   Whoo_Hoo  : ANSI.Graphic_Mode;

   Test_Message : constant String := "Hello! ";
begin -- General
   ANSI.Clear_Screen (Clear => ANSI.Completely);
   ANSI.Move_Cursor (X => 1, Y => 1, Move => ANSI.Absolute);

   for Color in ANSI.Color loop
      ANSI.Move_Cursor (X => Test_Message'Length * ANSI.Color'Pos (Color), Y => 1, Move => ANSI.Absolute);
      Mode.Foreground := Color;
      Mode.Bold       := True;
      Mode.Underline  := True;
      ANSI.Set_Graphic_Mode (Mode => Mode);
      Ada.Text_IO.Put_Line (Item => Test_Message);

      ANSI.Move_Cursor (X => Test_Message'Length * ANSI.Color'Pos (Color), Y => 2, Move => ANSI.Absolute);
      Mode.Underline := False;
      ANSI.Set_Graphic_Mode (Mode => Mode);
      Ada.Text_IO.Put_Line (Item => Test_Message);

      ANSI.Move_Cursor (X => Test_Message'Length * ANSI.Color'Pos (Color), Y => 3, Move => ANSI.Absolute);
      Mode.Bold := False;
      ANSI.Set_Graphic_Mode (Mode => Mode);
      Ada.Text_IO.Put_Line (Item => Test_Message);

      ANSI.Move_Cursor (X => Test_Message'Length * ANSI.Color'Pos (Color), Y => 4, Move => ANSI.Absolute);
      Mode.Foreground := ANSI.Default;
      ANSI.Set_Graphic_Mode (Mode => Mode);
      Ada.Text_IO.Put_Line (Item => Test_Message);
   end loop;

   Ada.Text_IO.New_Line;

   Random_Colors.Reset (Color_Gen);
   Random_Characters.Reset (Char_Gen);

   for Row in 1 .. 4 loop
      for Column in 1 .. Test_Message'Length * (ANSI.Color'Pos (ANSI.Color'Last) + 1) loop
         Mode.Background := Random_Colors.Random (Color_Gen);
         Mode.Foreground := Random_Colors.Random (Color_Gen);

         ANSI.Set_Graphic_Mode (Mode);

         Ada.Text_IO.Put (Random_Characters.Random (Char_Gen) );
      end loop;

      Ada.Text_IO.New_Line;
   end loop;

   ANSI.Reset_Graphic_Mode;

   Ada.Text_IO.New_Line;

   Whoo_Hoo.Bold := True;

   for Color in ANSI.Color loop
      Whoo_Hoo.Foreground := Color;
      ANSI.Set_Graphic_Mode (Whoo_Hoo);
      ANSI_Color_IO.Put (Color);
      Ada.Text_IO.Put (' ');
   end loop;

   Ada.Text_IO.New_Line;

   ANSI.Reset_Graphic_Mode;

   loop
      Ada.Text_IO.Put ("Choose A Color: ");

      begin
         ANSI_Color_IO.Get (Put_Color);

         exit;
      exception
         when Ada.IO_Exceptions.Data_Error =>
            Ada.Text_IO.Put ("Don't be cheeky. ");
      end;
   end loop;

   Ada.Text_IO.Put ("You have chosen: ");

   Whoo_Hoo.Foreground := Put_Color;

   ANSI.Set_Graphic_Mode (Whoo_Hoo);
   ANSI_Color_IO.Put (Put_Color);

   ANSI.Reset_Graphic_Mode;

   Ada.Text_IO.New_Line;

   ANSI.Reset_Graphic_Mode;
end General;
