
package ANSI is

   -- Type Name   : Clear_Type
   -- Description : Used to determine the extent of a procedure which
   --             : clears some part of a screen or line.
   -- Used By     : procedure Clear_Screen
   --             : procedure Clear_Line
   type Clear_Type is (To_Screen_End, To_Screen_Beginning, Entire_Screen, To_Line_End, To_Line_Beginning, Entire_Line);
   subtype Clear_Screen_Type is Clear_Type range To_Screen_End .. Entire_Screen;
   subtype Clear_Line_Type is Clear_Type range To_Line_End .. Entire_Line;

   type Scroll_Direction  is (Up, Down);

   -- Type Name   : Move_Type
   -- Description : Used to determine how the cursor should be moved.
   -- Used By     : procedure Move_Cursor
   type Move_Type is (Absolute, Relative);

   -- Type Name   : Cursor_Operation
   -- Description : Used to determine whether the cursor's position
   --             : should be saved or restored.
   -- Used By     : Cursor_Position
   type Cursor_Operation is (Save, Restore);

   -- Type Name   : ANSI_Graphic_Mode_Color
   -- Description : Used to define available ANSI X3.64 colors.
   -- Used By     : type ANSI_Graphic_Mode
   --             : type ANSI_Graphic_Mode_Color_Codes
   type Color is (Black, Red, Green, Yellow, Blue, Magenta, Cyan, White, Default);

   -- Type Name   : Graphic_Mode
   -- Description : Variables are create of this type to define custom
   --             : configurations of the available options.  The items
   --             : in the record are intuitively named, so there uses
   --             : will not be detailed, but please take note that every
   --             : feature is not supported by every terminal type.
   -- Used By     : Set_Graphic_Mode
   -- Depends On  : type Color
   type Graphic_Mode is record
      Foreground  : Color   := Default;
      Background  : Color   := Default;
      Bold        : Boolean := False;
      Blink       : Boolean := False;
      Rapid_Blink : Boolean := False;
      Reversed    : Boolean := False;
      Underline   : Boolean := False;
      Italic      : Boolean := False;
      Faint       : Boolean := False;
      Conceal     : Boolean := False;
      Cross_Out   : Boolean := False;
   end record;

   function "and" (Left, Right : Graphic_Mode) return Graphic_Mode;
   function "or" (Left, Right : Graphic_Mode) return Graphic_Mode;
   function "xor" (Left, Right : Graphic_Mode) return Graphic_Mode;

   ---------------------------------------------------------------------------
   -- All of the public subprograms.  These should allow great flexibility. --
   ---------------------------------------------------------------------------

   -- Function Name : Move_Cursor
   -- Description   : Will move the cursor relative to the cursor's location
   --               : at the time the procedure is called.  Depending on the
   --               : the value of MT, the cursor's position can be changed
   --               : with respect to its current position or from the "origin"
   --               : of the character-imaging device.
   -- Parameters    : X  : Integer;
   --               : Y  : Integer;
   --               : MT : Move_Type;
   -- Depends On    : type Move_Type
   --               : variable CSI
   --               : package Ada.Text_IO
   procedure Move_Cursor
     (X    : Integer;
      Y    : Integer;
      Move : Move_Type := Absolute);

   -- Function Name : Set_Graphic_Mode
   -- Description   : Outputs necessary data to screen to achieve effects
   --               : defined by components of Graphic_Mode.  Also, please,
   --               : take note that before doing so, Reset_ANSI_Graphic_Mode
   --               : is called.
   -- Parameters    : Graphic_Mode : ANSI_Graphic_Mode
   -- Depends On    : type ANSI_Graphic_Mode
   --               : package Ada.Strings.Unbounded
   --               : variable CSI
   --               : package Ada.Text_IO
   procedure Set_Graphic_Mode (Mode : Graphic_Mode);

   --
   --Function Name      : Reset_ANSI_Graphic_Mode
   --Description        : Outpus necessary ANSI code for putting display settings
   --           : to defaults.
   --Depends On : variable CSI
   --           : package Ada.Text_IO
   --
   procedure Reset_Graphic_Mode;

   procedure Clear (Clear : Clear_Type);

   -- Function Name : Clear_Screen
   -- Description   : Clears the screen to different extents depending
   --               : on the value of CT.  A value of To_End will clear
   --               : the screen from the current cursor position until
   --               : the end of the screen.  A value of To_Beginning
   --               : will clear the screen from the current cursor
   --               : position until the beginning.  A value of
   --               : Completely will clear the entire screen while
   --               : leaving the cursor position intact.  If the user
   --               : would like to place the cursor at the origin
   --               : he/she will need to take care of that on his/her own.
   -- Parameters    : CT : Clear_Type
   -- Depends On    : type Clear_Type
   --               : variable CSI
   --               : package Ada.Text_IO
   procedure Clear_Screen (Clear : Clear_Screen_Type);

   -- Function Name : Clear_Line
   -- Description   : Clears the line to different extents depending
   --               : on the value of CT.  A value of To_End will clear
   --               : the line from the current cursor position until
   --               : the end of the line.  A value of To_Beginning
   --               : will clear the line from the current cursor
   --               : position until the beginning of the line.  A value of
   --               : Completely will clear the entire line while
   --               : leaving the cursor position intact.  If the user
   --               : would like to place the cursor at the beginning
   --               : he/she will need to take care of that on his/her own.
   -- Parameters    : CT : Clear_Type
   -- Depends On    : type Clear_Type
   --               : variable CSI
   --               : package Ada.Text_IO
   procedure Clear_Line (Clear : Clear_Line_Type);

   procedure Get_Cursor_Position (Column, Line : out Natural);

   procedure Get_Screen_Size
      (Lines   : out Natural;
       Columns : out Natural);

   -- Function Name : Cursor_Position
   -- Description   :
   -- Parameters    : CO : Cursor Operation
   -- Depends On    : package Ada.Text_IO;
   --               : type Cursor_Operation
   procedure Cursor_Position (Operation : Cursor_Operation);

--   procedure Scroll (Direction : Scroll_Direction; Lines : Natural := 1);
end ANSI;
