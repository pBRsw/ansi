with Ada.Characters.Latin_1;
with Ada.Text_IO;
with Ada.Strings.Fixed;
with Ada.Strings.Unbounded;

package body ANSI is

   -- Variable Name : Control_Sequence_Introducer
   -- Description   : The long-winded name comes directly from the
   --               : standard.  Used in functions at the beginning
   --               : of ANSI output to signal to a terminal exactly
   --               : what procedes it.
   -- Used By       : variable CSI
   Control_Sequence_Introducer : constant String := Ada.Characters.Latin_1.ESC & '[';

   -- Variable Name : CSI
   -- Description   : With Control_Sequence_Identifier being long-
   --               : winded, CSI was introduced to make life easier.
   -- Used By       : procedure Move_Cursor
   --               : procedure Set_ANSI_Graphic_Mode
   --               : procedure Reset_ANSI_Graphic_Mode
   --               : procedure Clear_Screen
   --               : procedure Clear_Line
   --               : procedure Get_Cursor_Position
   --               : procedure Cursor_Position
   CSI : String renames Control_Sequence_Introducer;

   -- Type Name  : ANSI_Graphic_Mode_Color_Codes
   -- Description        : Creates an array with an index defined by
   --            : ANSI_Graphic_Mode_Color of strings of length 2
   --            : for use in defining ANSI colors codes for ouptut
   --            : to a character-imaging device.
   -- Depends On : type ANSI_Graphic_Mode_Color
   -- Used By    : variable ANSI_Graphic_Mode_Colors_Foreground
   --            : variable ANSI_Graphic_Mode_Colors_Background
   type Color_Code_Association is array (Color) of Integer;

   -- Variable Name      : ANSI_Graphic_Mode_Colors_Foreground
   -- Description        : Instantiates a constant ANSI_Graphic_Mode_Color_Codes
   --            : to define ANSI color codes for the foreground.
   -- Depends On : type ANSI_Graphic_Mode_Color_Codes
   -- Used By    : procedure Set_ANSI_Graphic_Mode
   Foreground_Color_Code : constant Color_Code_Association :=
     (Black   => 30,
      Red     => 31,
      Green   => 32,
      Yellow  => 33,
      Blue    => 34,
      Magenta => 35,
      Cyan    => 36,
      White   => 37,
      Default => 39);

   -- Variable Name      : ANSI_Graphic_Mode_Colors_Background
   -- Description        : Instantiates a constant ANSI_Graphic_Mode_Color_Codes
   --            : to define ANSI color codes for the background.
   -- Depends On : type ANSI_Graphic_Mode_Color_Codes
   -- Used By    : procedure Set_ANSI_Graphic_Mode
   Background_Color_Code : constant Color_Code_Association :=
     (Black   => 40,
      Red     => 41,
      Green   => 42,
      Yellow  => 43,
      Blue    => 44,
      Magenta => 45,
      Cyan    => 46,
      White   => 47,
      Default => 49);

   -- -----------------------------------------
   -- Local functions for my own personal use.-
   -- -----------------------------------------

   -- Function Name      : Img
   -- Description        : Essentially, it is a wrapper for Integer'Image.
   -- Parameters : Number : Integer;
   -- Return Value       : String
   -- Used By    : procedure Move_Cursor
   --            : procedure Set_ANSI_Graphic_Mode
   --            : procedure Reset_ANSI_Graphic_Mode
   --            : procedure Clear_Screen
   --            : procedure Clear_Line
   --            : procedure Cursor_Position
   function Image (Item : Integer) return String;

   function Image (Item : Integer) return String is
   begin -- Image
      return Ada.Strings.Fixed.Trim (Source => Integer'Image (Item), Side => Ada.Strings.Left);
   end Image;

   --
   -- Exported Functions are documented in the specification.
   --
   -- ---------------------------
   -- Functions for general use.-
   -- ---------------------------

   function "and" (Left, Right : Graphic_Mode) return Graphic_Mode is
   begin
      return Graphic_Mode'
               (Foreground  => Left.Foreground,
                Background  => Left.Background,
                Bold        => Left.Bold        and Right.Bold,
                Blink       => Left.Blink       and Right.Blink,
                Rapid_Blink => Left.Rapid_Blink and Right.Rapid_Blink,
                Reversed    => Left.Reversed    and Right.Reversed,
                Underline   => Left.Underline   and Right.Underline,
                Italic      => Left.Italic      and Right.Italic,
                Faint       => Left.Faint       and Right.Faint,
                Conceal     => Left.Conceal     and Right.Conceal,
                Cross_Out   => Left.Cross_Out   and Right.Cross_Out);
   end "and";

   function "or" (Left, Right : Graphic_Mode) return Graphic_Mode is
   begin
      return Graphic_Mode'
               (Foreground  => Left.Foreground,
                Background  => Left.Background,
                Bold        => Left.Bold        or Right.Bold,
                Blink       => Left.Blink       or Right.Blink,
                Rapid_Blink => Left.Rapid_Blink or Right.Rapid_Blink,
                Reversed    => Left.Reversed    or Right.Reversed,
                Underline   => Left.Underline   or Right.Underline,
                Italic      => Left.Italic      or Right.Italic,
                Faint       => Left.Faint       or Right.Faint,
                Conceal     => Left.Conceal     or Right.Conceal,
                Cross_Out   => Left.Cross_Out   or Right.Cross_Out);
   end "or";

   function "xor" (Left, Right : Graphic_Mode) return Graphic_Mode is
   begin
      return Graphic_Mode'
               (Foreground  => Left.Foreground,
                Background  => Left.Background,
                Bold        => Left.Bold        xor Right.Bold,
                Blink       => Left.Blink       xor Right.Blink,
                Rapid_Blink => Left.Rapid_Blink xor Right.Rapid_Blink,
                Reversed    => Left.Reversed    xor Right.Reversed,
                Underline   => Left.Underline   xor Right.Underline,
                Italic      => Left.Italic      xor Right.Italic,
                Faint       => Left.Faint       xor Right.Faint,
                Conceal     => Left.Conceal     xor Right.Conceal,
                Cross_Out   => Left.Cross_Out   xor Right.Cross_Out);
   end "xor";

   procedure Move_Cursor
     (X    : Integer;
      Y    : Integer;
      Move : Move_Type := Absolute)
   is
      Output : Ada.Strings.Unbounded.Unbounded_String;

      use type Ada.Strings.Unbounded.Unbounded_String;
   begin -- Move_Cursor
      if Move = Absolute then
         Output := Output & CSI & Image (Y) & ';' & Image (X) & 'f';
      elsif Move = Relative then
         if X /= 0 then
            Output := Output & CSI & Image (X);

            if X > 0 then
               Output := Output & 'C';
            else
               Output := Output & 'D';
            end if;
         end if;

         if Y /= 0 then
            Output := Output & CSI & Image (Y);

            if Y > 0 then
               Output := Output & 'A';
            else
               Output := Output & 'C';
            end if;
         end if;
      end if;

      Ada.Text_IO.Put (Ada.Strings.Unbounded.To_String (Output) );
   end Move_Cursor;

   procedure Set_Graphic_Mode (Mode : Graphic_Mode) is
      Graphic_Mode_Set : Boolean := False;

      Output : Ada.Strings.Unbounded.Unbounded_String;

      procedure Semis;

      procedure Semis is
         use type Ada.Strings.Unbounded.Unbounded_String;
      begin -- Semis
         if Graphic_Mode_Set then
            Output := Output & ';';
         else
            Graphic_Mode_Set := True;
         end if;
      end Semis;

      use type Ada.Strings.Unbounded.Unbounded_String;
   begin -- Set_Graphic_Mode
      Reset_Graphic_Mode;

      for Option in 1 .. 11 loop -- What are 1 .. 11?  Consider a ranged type.
         case Option is
            when 1  =>
               Semis;
               Output := Output & Image (Foreground_Color_Code (Mode.Foreground) );
            when 2  =>
               Semis;
               Output := Output & Image (Background_Color_Code (Mode.Background) );
            when 3  =>
               if Mode.Bold then
                  Semis;
                  Output := Output & Image (1);
               end if;
            when 4  =>
               if Mode.Blink then
                  Semis;
                  Output := Output & Image (5);
               end if;
            when 5  =>
               if Mode.Rapid_Blink then
                  Semis;
                  Output := Output & Image (6);
               end if;
            when 6  =>
               if Mode.Reversed then
                  Semis;
                  Output := Output & Image (7);
               end if;
            when 7  =>
               if Mode.Underline then
                  Semis;
                  Output := Output & Image (4);
               end if;
            when 8  =>
               if Mode.Italic then
                  Semis;
                  Output := Output & Image (3);
               end if;
            when 9  =>
               if Mode.Faint then
                  Semis;
                  Output := Output & Image (2);
               end if;
            when 10 =>
               if Mode.Conceal then
                  Semis;
                  Output := Output & Image (8);
               end if;
            when 11 =>
               if Mode.Underline then
                  Semis;
                  Output := Output & Image (9);
               end if;
         end case;
      end loop;

      if Graphic_Mode_Set then
         Output := CSI & Output & 'm';
      end if;

      Ada.Text_IO.Put (Ada.Strings.Unbounded.To_String (Output) );
   end Set_Graphic_Mode;

   procedure Reset_Graphic_Mode is
   begin -- Reset_Graphic_Mode
      Ada.Text_IO.Put (CSI & Image (0) & 'm');
   end Reset_Graphic_Mode;

   procedure Clear (Clear : Clear_Type) is
   begin -- Clear
      Ada.Text_IO.Put (CSI & Image ( (Clear_Type'Pos (Clear) ) rem 3) );

      if Clear in To_Screen_End .. Entire_Screen then
         Ada.Text_IO.Put ('J');
      else
         Ada.Text_IO.Put ('K');
      end if;
   end Clear;

   procedure Clear_Screen (Clear : Clear_Screen_Type) is
   begin -- Clear_Screen
      Ada.Text_IO.Put (CSI & Image (Clear_Type'Pos (Clear) ) & 'J');

      Move_Cursor (X => 0, Y => 0, Move => Absolute);
   end Clear_Screen;

   procedure Clear_Line (Clear : Clear_Line_Type) is
   begin -- Clear_Line
      Ada.Text_IO.Put (CSI & Image (Clear_Type'Pos (Clear) ) & 'K');
   end Clear_Line;

   procedure Get_Cursor_Position (Column, Line : out Natural) is
      Buffer : Character;
   begin -- Get_Cursor_Position
      Column := 0;
      Line   := 0;

      Ada.Text_IO.Put (CSI & "6n");

      Ada.Text_IO.Get_Immediate (Buffer);
      Ada.Text_IO.Get_Immediate (Buffer);

      loop
         Ada.Text_IO.Get_Immediate (Buffer);

         exit when Buffer = ';';

         Line := Line * 10 + (Character'Pos (Buffer) - Character'Pos ('0') );
      end loop;

      -- Accumulate columns parameter
      loop
         Ada.Text_IO.Get_Immediate (Buffer);

         exit when Buffer = 'R';

         Column := Column * 10 + (Character'Pos (Buffer) - Character'Pos ('0') );
      end loop;
   end Get_Cursor_Position;

   procedure Get_Screen_Size
      (Lines   : out Natural;
       Columns : out Natural)
   is
      Buffer : Character;
   begin  -- GetScreenSize
      Lines   := 0;
      Columns := 0;

      -- Ask for text area dimensions
      Ada.Text_IO.Put (CSI & "18t");

      -- Ignore initial "ESC [ 8 ;"
      loop
         Ada.Text_IO.Get_Immediate (Buffer);

         exit when Buffer = ';';
      end loop;

      -- Accumulate lines parameter
      loop
         Ada.Text_IO.Get_Immediate (Buffer);

         exit when Buffer = ';';

         Lines := Lines * 10 + (Character'Pos (Buffer) - Character'Pos ('0') );
      end loop;

      -- Accumulate columns parameter
      loop
         Ada.Text_IO.Get_Immediate (Buffer);

         exit when Buffer = 't';

         Columns := Columns * 10 + (Character'Pos (Buffer) - Character'Pos ('0') );
      end loop;
   end Get_Screen_Size;

   procedure Cursor_Position (Operation : Cursor_Operation) is
   begin -- Cursor_Position
      if Operation = Save then
         Ada.Text_IO.Put (CSI & 's');
      elsif Operation = Restore then
         Ada.Text_IO.Put (CSI & 'u');
      end if;
   end Cursor_Position;

--   procedure Scroll (Direction : Scroll_Direction, Lines : Natural := 1) is
--   begin
--      Ada.Text_IO.Put (CSI & '
end ANSI;
